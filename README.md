We do not know what are the difference between these z-values:
	-Left
		*Gaze Direction
		*Gaze Origin
		*GazeRayWorld
			~Origin
			~Direction

	-Right
		*Gaze Direction
		*Gaze Origin
		*GazeRayWorld
			~Origin
			~Direction

	-CombinedGazeRayWorld
		*Direction
		*Origin

	-LeftEye
		*Unitvector
		*PositionInHMDCoordinates
		
	-RightEye
		*Unitvector
		*PositionInHMDCoordinates


Also, we want to confirm what PositionInTrackingArea means for both LeftEye and RightEye.

Finally, we want to know if tobii XR is better at getting data from the eyetracking than tobii Pro.
		
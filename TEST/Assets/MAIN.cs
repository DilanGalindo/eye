﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Tobii.Research.Unity.Examples;
using Tobii.Research.Unity;

public class MAIN : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Attach 3D text object here.")]
    private TextMesh _threeDText;

    private VREyeTracker _eyeTracker;
    private VRGazeTrail _gazeTrail;
    private VRCalibration _calibration;
    private VRSaveData _saveData;
    private VRPositioningGuide _positioningGuide;
    private Color _particleColor;
    private string path;
    private string nametime;

    private string[] list;
    // Start is called before the first frame update
    void CreateText()
    {
        //Path of the file
        nametime = System.DateTime.Now.ToString("hh-mm-ss-tt-MM-dd-yyyy");

        path = @"C:\Data Collection\" + nametime + "-eyeTrackerdData.txt";
        if (!File.Exists(path))
        {
            //list = new string[] { "Keycode        Time           X-Position     Y-Position     Z-Position     X-Rotation     Y-Rotation     Z-Rotation     " };

            File.WriteAllText(path, "Time \t \t Left\t X-Position \t Y-Position \t Z-Position \t X-Position \t Y-Position \t Z-Position " +
                                  "\t\tRight\t X-Position \t Y-Position \t Z-Position \t X-Position \t Y-Position \t Z-Position " +
                                  "\t\tCombined\t X-Position \t Y-Position \t Z-Position \t X-Position \t Y-Position \t Z-Position \n");


        }
            
    }

    void Start()
    {
        CreateText();
        // Cache our prefab scripts.
        _eyeTracker = VREyeTracker.Instance;
        _gazeTrail = VRGazeTrail.Instance;
        _calibration = VRCalibration.Instance;
        _saveData = VRSaveData.Instance;
        _positioningGuide = VRPositioningGuide.Instance;

        // Move HUD to be in front of user.
        var etOrigin = VRUtility.EyeTrackerOriginVive;
        var holder = _threeDText.transform.parent;
        holder.parent = etOrigin;
        holder.localPosition = new Vector3(0, -1.35f, 3);
        holder.localRotation = Quaternion.Euler(25, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        // We are expecting to have all objects.
        //    if (!_eyeTracker || !_calibration || !_saveData )
        //    {
        //      return;
        //  }

        // Thin out updates a bit.
        //if (Time.frameCount % 9 != 0)
        //{
        //    return;
        //}
        list = new string[] { "Keycode        Time           X-Position     Y-Position     Z-Position     X-Rotation     Y-Rotation     Z-Rotation     ", _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.direction.x.ToString() };
        if (Input.GetKeyUp(KeyCode.S)) //saves data on command, emergency save function
        {
            System.IO.File.WriteAllLines(@"C:\Data Collection\" + nametime + "-data.txt", list);
        }
        string realtime = System.DateTime.Now.ToString("hh:mm:ss tt");

        string LeftDirX = _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.direction.x.ToString();
        string LeftDirY = _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.direction.y.ToString();
        string LeftDirZ = _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.direction.z.ToString();

        string RightDirX = _eyeTracker.LatestProcessedGazeData.Right.GazeRayWorld.direction.x.ToString();
        string RightDirY = _eyeTracker.LatestProcessedGazeData.Right.GazeRayWorld.direction.y.ToString();
        string RightDirZ = _eyeTracker.LatestProcessedGazeData.Right.GazeRayWorld.direction.z.ToString();
        

        string LeftOriginX = _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.origin.x.ToString();
        string LeftOriginY = _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.origin.y.ToString();
        string LeftOriginZ = _eyeTracker.LatestProcessedGazeData.Left.GazeRayWorld.origin.z.ToString();

        string RightOriginX = _eyeTracker.LatestProcessedGazeData.Right.GazeRayWorld.origin.x.ToString();
        string RightOriginY = _eyeTracker.LatestProcessedGazeData.Right.GazeRayWorld.origin.y.ToString();
        string RightOriginZ = _eyeTracker.LatestProcessedGazeData.Right.GazeRayWorld.origin.z.ToString();


        string CombinedDirX = _eyeTracker.LatestProcessedGazeData.CombinedGazeRayWorld.direction.x.ToString();
        string CombinedDirY = _eyeTracker.LatestProcessedGazeData.CombinedGazeRayWorld.direction.y.ToString();
        string CombinedDirZ = _eyeTracker.LatestProcessedGazeData.CombinedGazeRayWorld.direction.z.ToString();

        string CombinedOriginX = _eyeTracker.LatestProcessedGazeData.CombinedGazeRayWorld.origin.x.ToString();
        string CombinedOriginY = _eyeTracker.LatestProcessedGazeData.CombinedGazeRayWorld.origin.y.ToString();
        string CombinedOriginZ = _eyeTracker.LatestProcessedGazeData.CombinedGazeRayWorld.origin.z.ToString();

       string[] GazeDATA = {realtime, LeftDirX+"\t"+ LeftDirY+"\t" +LeftDirZ+"\t",
                                     LeftOriginX+"\t"+ LeftOriginY+"\t"+ LeftOriginZ +"\t",
                            
                                     RightDirX+"\t"+ RightDirY+"\t"+ RightDirZ+"\t",
                                     RightOriginX+"\t"+ RightOriginY+"\t"+ RightOriginZ+"\t",

                                    CombinedDirX+"\t"+ CombinedDirY+"\t"+ CombinedDirZ+"\t",
                                    CombinedOriginX+"\t"+ CombinedOriginY+"\t"+ CombinedOriginZ+"\t"};

        /*
        IEnumerable<string> GazeDATA = {realtime+ "      \t" + LeftDirX + '\t'+ LeftDirY + '\t' + LeftDirZ+'\t'+
                                     LeftOriginX + '\t' + LeftOriginY + '\t' + LeftOriginZ + "\t\t" +

                                     RightDirX + '\t' + RightDirY + '\t' + RightDirZ + '\t'+
                                     RightOriginX + '\t' + RightOriginY + '\t' + RightOriginZ + "\t\t"+

                                    CombinedDirX + '\t' + CombinedDirY + '\t' + CombinedDirZ + '\t'
                                    + CombinedOriginX + '\t' + CombinedOriginY + '\t' + CombinedOriginZ+"\n"};
                                    */
        File.AppendAllLines(path, GazeDATA);

        // Create an informational string.

    }
}

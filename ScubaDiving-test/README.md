# ScubaDiving

> The human brain is ingenious and mysterious, but every day we get a little closer to understanding it. Our research seeks to shed light on the furthest corners of our minds.
>Human beings, like many animals, mentally map their surroundings using specific neurons called “grid cells.” Each grid cell corresponds with a physical location and will fire when the person or animal is in that location. In a previous study, Dr. Zoltan Nadasdy investigated grid cell activity in people navigating a 2-dimensional environments by playing video games. Our current research investigates grid cell activity in a 3-dimensional virtual world.

<p>In this project, we build a virtual scubadiving experience using Unity 3D. The project has been tested to be compatible with Oculus Rift and HTC Vive. </p>

To learn more about our research project,

 - Read our [project abstract](https://github.com/Xieyangxinyu/ScubaDiving/tree/master/Abstract) on Capital of Texas Undergraduate Research Conference.
 - Watch our [Texas Student Research Showdown Video](https://www.youtube.com/watch?v=_50E9YZ02-Q&feature=youtu.be).
 
 ### Lead Researcher
 - [Dr. Zoltan Nadasdy](https://brainstim.psy.utexas.edu/?page_id=19)
 - Dr. Darrell Haufler
 ### Undergradute Researchers
 - Jacob M Quinn
 - Jiayi Zhou
 - Ricardo J Velásquez
 - Yangxinyu Xie
 
#### Asset Credit to:
 - Moonflower Carnivore
 - Nobiax / Yughues
 - janpec
 - Rivermill Studios

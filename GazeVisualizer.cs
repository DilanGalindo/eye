﻿// Copyright © 2018 – Property of Tobii AB (publ) - All Rights Reserved

using System.IO;
using UnityEngine;

namespace Tobii.XR.GazeVisualizer
{

    public class GazeVisualizer : MonoBehaviour
    {
        private static string nametime = System.DateTime.Now.ToString("hh-mm-ss-tt-MM-dd-yyyy");
        private string path = @"C:\Data Collection\" + nametime + "-VReyeTrackerdData.txt";
        private TobiiXR_GazeRay eyetrackerGaze;
        private TobiiXR_EyeTrackingData eyetracker;
        public enum GazeVisualizerType
        {
            Default,
            Bubble,
        }

        public IEyeTrackingProvider EyetrackingProvider { get; set; }

        public bool ScaleAffectedByPrecision;

#pragma warning disable 649
        [SerializeField] private GazeVisualizerType _visualizerType;

        [SerializeField] private bool _smoothMove = true;

        [SerializeField] [Range(1, 30)] private int _smoothMoveSpeed = 7;
#pragma warning restore 649

        private float ScaleFactor
        {
            get { return _visualizerType == GazeVisualizerType.Bubble ? 0.03f : 0.003f; }
        }

        private float _defaultDistance;

        private Camera _mainCamera;

        private SpriteRenderer _spriteRenderer;
        private Vector3 _lastGazeDirection;

        private const float OffsetFromFarClipPlane = 10f;
        private const float PrecisionAngleScaleFactor = 5f;

        private void Start()
        {
            _mainCamera = CameraHelper.GetMainCamera();
            _spriteRenderer = GetComponent<SpriteRenderer>();

            _defaultDistance = _mainCamera.farClipPlane - OffsetFromFarClipPlane;

            if (!File.Exists(path))
            {
                //list = new string[] { "Keycode        Time           X-Position     Y-Position     Z-Position     X-Rotation     Y-Rotation     Z-Rotation     " };

                File.WriteAllText(path, "Date-Time\tX-Environment\tY-Environment\tZ-Environment\t\n");


            }
        }

        void Update()
        {
            var provider = EyetrackingProvider ?? TobiiXR.Provider;
            var gazeModifierProvider = provider as IGazeModifierProvider;

            var eyeTrackingData = ScaleAffectedByPrecision && gazeModifierProvider != null
                ? gazeModifierProvider.AccuracyOnlyModifiedEyeTrackingData
                : provider.EyeTrackingData;
            var gazeRay = eyeTrackingData.GazeRay;


            _spriteRenderer.enabled = gazeRay.IsValid;

            if (_spriteRenderer.enabled == false) return;

            SetPositionAndScale(gazeRay);

            if (ScaleAffectedByPrecision)
            {
                UpdatePrecisionScale(gazeModifierProvider);
            }
        }

        public void SetPositionAndScale(TobiiXR_GazeRay gazeRay)
        {
            string realtime = System.DateTime.Now.ToString("hh:mm:ss tt");

            RaycastHit hit;
            var distance = _defaultDistance;
            if (Physics.Raycast(gazeRay.Origin, gazeRay.Direction, out hit))
            {
                distance = hit.distance;
            }

            var interpolatedGazeDirection = Vector3.Lerp(_lastGazeDirection, gazeRay.Direction,
                _smoothMoveSpeed * Time.unscaledDeltaTime);

            var usedDirection = _smoothMove ? interpolatedGazeDirection.normalized : gazeRay.Direction.normalized;
            transform.position = gazeRay.Origin + usedDirection * distance;

            transform.localScale = Vector3.one * distance * ScaleFactor;

            transform.forward = usedDirection.normalized;


            _lastGazeDirection = usedDirection;
            string list =
                realtime + "\t" +
                hit.point.x.ToString("0.0000000") + "\t" + hit.point.y.ToString("0.0000000") + "\t" + distance.ToString("0.0000000") + "\t\n"
                        ;
            if (!distance.ToString("0.0000000").Equals("990.0000000"))
            {
                System.IO.File.AppendAllText(path, list);

            }
        }

        private void UpdatePrecisionScale(IGazeModifierProvider gazeModifierProvider)
        {
            if (gazeModifierProvider == null) return;

            transform.localScale *= (1f + GetScaleAffectedByPrecisionAngle(gazeModifierProvider));
        }

        private static float GetScaleAffectedByPrecisionAngle(IGazeModifierProvider gazeModifierProvider)
        {
            return gazeModifierProvider.MaxPrecisionAngleDegrees * Mathf.Sin(gazeModifierProvider.MaxPrecisionAngleDegrees * Mathf.Deg2Rad) * PrecisionAngleScaleFactor;
        }
    }
}